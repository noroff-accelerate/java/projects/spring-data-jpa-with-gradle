package no.acclerate.hibernatetestgradle.repositories;

import no.acclerate.hibernatetestgradle.models.Professor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository (DAO) for the Professor domain class.
 * Uses @Query for business logic that is difficult to achieve with default functionality.
 */
@Repository
public interface ProfessorRepository extends JpaRepository<Professor, Integer> {
}
