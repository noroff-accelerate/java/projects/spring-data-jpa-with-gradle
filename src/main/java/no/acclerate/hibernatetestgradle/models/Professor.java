package no.acclerate.hibernatetestgradle.models;

import javax.persistence.*;
import java.util.Set;

/**
 * Domain class (entity) to represent a Professor.
 * Includes an auto generated key and some validation.
 * Relationships are configured as default, so collections are lazily loaded.
 */
@Entity
public class Professor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 50, nullable = false)
    private String name;
    @OneToMany(mappedBy = "professor")
    private Set<Student> students;
    @OneToMany(mappedBy = "professor")
    private Set<Subject> subjects;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Student> getStudents() {
        return students;
    }

    public void setStudents(Set<Student> students) {
        this.students = students;
    }

    public Set<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(Set<Subject> subjects) {
        this.subjects = subjects;
    }
}
