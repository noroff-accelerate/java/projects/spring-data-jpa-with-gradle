package no.acclerate.hibernatetestgradle.runners;

import no.acclerate.hibernatetestgradle.services.ProfessorService;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * Client class for the Services.
 * This class exists to demonstrate the use of services.
 * It implements ApplicationRunner to be able to use dependency injection.
 */
@Component
public class AppRunner implements ApplicationRunner {

    private final ProfessorService professorService;

    public AppRunner(ProfessorService professorService) {
        this.professorService = professorService;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        professorService.deleteById(1);
    }
}
