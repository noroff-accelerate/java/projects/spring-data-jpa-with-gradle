package no.acclerate.hibernatetestgradle.services;

import no.acclerate.hibernatetestgradle.models.Student;
import no.acclerate.hibernatetestgradle.repositories.StudentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * Implementation of the Student service.
 * Uses the Student repository to interact with the data store.
 * Logs errors through the standard logger.
 */
@Service
public class StudentServiceImpl implements StudentService {
    private final StudentRepository studentRepository;
    private final Logger logger = LoggerFactory.getLogger(StudentServiceImpl.class);

    public StudentServiceImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public Student findById(Integer id) {
        return studentRepository.findById(id).get();
    }

    @Override
    public Collection<Student> findAll() {
        return studentRepository.findAll();
    }

    @Override
    public Student add(Student entity) {
        return studentRepository.save(entity);
    }

    @Override
    public Student update(Student entity) {
        return studentRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {
        studentRepository.deleteById(id);
    }

    @Override
    public void setProfessor(int studentId, int professorId) {
        studentRepository.updateProfessorById(studentId,professorId);
    }

    @Override
    public void setProject(int studentId, int projectId) {
        studentRepository.updateProjectById(studentId,projectId);
    }

    @Override
    public Collection<Student> findAllByName(String name) {
        return studentRepository.findAllByName(name);
    }
}
