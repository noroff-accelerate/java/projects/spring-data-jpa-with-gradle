package no.acclerate.hibernatetestgradle.services;

import no.acclerate.hibernatetestgradle.models.Student;
import java.util.Collection;

/**
 * Service for the Student domain class.
 * Providing basic CRUD functionality through CrudService and any extended functionality.
 */
public interface StudentService extends CrudService<Student, Integer> {
    void setProfessor(int studentId, int professorId);
    void setProject(int studentId, int projectId);
    Collection<Student> findAllByName(String name);
}
