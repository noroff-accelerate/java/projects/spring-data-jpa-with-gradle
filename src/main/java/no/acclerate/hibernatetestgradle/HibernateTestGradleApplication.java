package no.acclerate.hibernatetestgradle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HibernateTestGradleApplication {

    public static void main(String[] args) {
        SpringApplication.run(HibernateTestGradleApplication.class, args);
    }
}
